from django.contrib import admin
from .models import *

class PriceBitovkaAdmin (admin.ModelAdmin):
	list_display = [field.name for field in PriceBitovka._meta.fields]

	class Meta:
		model = PriceBitovka

admin.site.register(PriceBitovka, PriceBitovkaAdmin)

class ImgBitovkaAdmin (admin.ModelAdmin):
	list_display = [field.name for field in ImgBitovka._meta.fields]

	class Meta:
		model = ImgBitovka

admin.site.register(ImgBitovka, ImgBitovkaAdmin)


class CharacteristicBitovkaAdmin (admin.ModelAdmin):
	list_display = [field.name for field in CharacteristicBitovka._meta.fields]

	class Meta:
		model = CharacteristicBitovka

admin.site.register(CharacteristicBitovka, CharacteristicBitovkaAdmin)
# Register your models here.
