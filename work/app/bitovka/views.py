from django.shortcuts import render
from .models import *
from catalog.models import *
from by.models import *

def catalog(request):
	products = Bitovka.objects.all()

	raspashonka_5 = PriceBitovka.objects.filter(size = "5.0х2.3",  name = "4")
	raspashonka_6 = PriceBitovka.objects.filter(size = "6.0х2.3",  name = "4")
	raspashonka_7 = PriceBitovka.objects.filter(size = "7.0х2.3",  name = "4")
	raspashonka_img = ImgBitovka.objects.filter(name = "4")

	torcevaya_tamb_5 = PriceBitovka.objects.filter(size = "5.0х2.3",  name = "3")
	torcevaya_tamb_6 = PriceBitovka.objects.filter(size = "6.0х2.3",  name = "3")
	torcevaya_tamb_7 = PriceBitovka.objects.filter(size = "7.0х2.3",  name = "3")
	torcevaya_tamb_img = ImgBitovka.objects.filter(name = "3")

	fasadnaya_5 = PriceBitovka.objects.filter(size = "5.0х2.3",  name = "2")
	fasadnaya_6 = PriceBitovka.objects.filter(size = "6.0х2.3",  name = "2")
	fasadnaya_7 = PriceBitovka.objects.filter(size = "7.0х2.3",  name = "2")
	fasadnaya_img = ImgBitovka.objects.filter(name = "2")

	torcevaya_4 = PriceBitovka.objects.filter(size = "4.0х2.3",  name = "1")
	torcevaya_5 = PriceBitovka.objects.filter(size = "5.0х2.3",  name = "1")
	torcevaya_6 = PriceBitovka.objects.filter(size = "6.0х2.3",  name = "1")
	torcevaya_img = ImgBitovka.objects.filter(name = "1")



	by_img = ImgBy.objects.filter(is_active = True)
	return render(request, "bitovka.html", locals())


