from django.urls import path
from django.contrib import admin
from catalog.models import Bitovka
from bitovka import views
from bitovka.models import *

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import render
from django.views.generic import DetailView


urlpatterns = [
         path(r'', views.catalog, name = 'catalog'),
         path(r'<int:pk>',DetailView.as_view(model =Bitovka, template_name = "1_obiect.html"), name = 'bitovka_detail')
    ]\
            + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
            + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)