from django.apps import AppConfig


class BitovkaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bitovka'

    verbose_name = "Бытовки"