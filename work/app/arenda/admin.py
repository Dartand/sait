from django.contrib import admin
from .models import *

class ArendaAdmin (admin.ModelAdmin):
	list_display = [field.name for field in Arenda._meta.fields]

	class Meta:
		model = Arenda

admin.site.register(Arenda, ArendaAdmin)
# Register your models here.
