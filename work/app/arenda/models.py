from django.db import models


class Arenda(models.Model):
	name = models.CharField(max_length = 50)
	price = models.DecimalField(max_digits = 7, decimal_places = 0)
	image = models.ImageField(upload_to = "product images/")
	text = models.TextField(max_length = 1000)
	def __str__(self):
		return "%s" % self.name

	class Meta():
		verbose_name = "Аренда"
		verbose_name_plural = "Аренда"
# Create your models here.
