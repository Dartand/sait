from django.contrib import admin
from .models import *


class KomplektAdmin(admin.ModelAdmin):
	list_display = [field.name for field in Komplekt._meta.fields]

	class Meta:
		model = Komplekt

admin.site.register(Komplekt, KomplektAdmin)
# Register your models here.
