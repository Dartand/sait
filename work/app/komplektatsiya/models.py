from django.db import models


class Komplekt(models.Model):
	name =models.CharField(max_length = 50)
	price = models.DecimalField(max_digits = 6, decimal_places = 0)
	count = models.DecimalField(max_digits = 2, decimal_places = 0)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Комлектация"
		verbose_name_plural = "Комплектации"
# Create your models here.
