from django.apps import AppConfig


class KomplektatsiyaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'komplektatsiya'
