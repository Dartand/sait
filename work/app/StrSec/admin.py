from django.contrib import admin
from .models import *

class ImgModulStructureAdmin (admin.ModelAdmin):
	list_display = [field.name for field in ImgModulStructure._meta.fields]

	class Meta:
		model = ImgModulStructure

admin.site.register(ImgModulStructure, ImgModulStructureAdmin )

class ImgPostSecurityAdmin (admin.ModelAdmin):
	list_display = [field.name for field in ImgPostSecurity._meta.fields]

	class Meta:
		model = ImgPostSecurity

admin.site.register(ImgPostSecurity, ImgPostSecurityAdmin)

class CharacteristicModulStructureAdmin(admin.ModelAdmin):
	list_display = [field.name for field in CharacteristicModulStructure._meta.fields]

	class Meta:
		model = CharacteristicModulStructure

admin.site.register(CharacteristicModulStructure, CharacteristicModulStructureAdmin)