# Generated by Django 4.0.5 on 2022-07-01 11:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='ImgPostSecurity',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='product images/')),
                ('name', models.ForeignKey(blank=True, default=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.postsecurity')),
            ],
            options={
                'verbose_name': 'Фотграфия постов охраны',
                'verbose_name_plural': 'Фотографии постов охраны',
            },
        ),
        migrations.CreateModel(
            name='ImgModulStructure',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='product images/')),
                ('name', models.ForeignKey(blank=True, default=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.modulstructure')),
            ],
            options={
                'verbose_name': 'Фотграфия модульных задний',
                'verbose_name_plural': 'Фотографии модульных зданий',
            },
        ),
        migrations.CreateModel(
            name='CharacteristicModulStructure',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=250)),
                ('name', models.ForeignKey(blank=True, default=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='catalog.modulstructure')),
            ],
            options={
                'verbose_name': 'Характеристика Модульного здания',
                'verbose_name_plural': 'Характеристики  Модульного здания',
            },
        ),
    ]
