from django.apps import AppConfig


class StrsecConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'StrSec'
    verbose_name = "Посты охраны и модульные здания"