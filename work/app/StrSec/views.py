from django.shortcuts import render
from catalog.models import ModulStructure, PostSecurity
from by.models import *
from .models import *


def catalog_str(request):
	Modul_str_5 = ModulStructure.objects.filter(id = "5")
	Modul_str_5_img = ImgModulStructure.objects.filter(name = "5")
	Modul_str_4 = ModulStructure.objects.filter(id = "4")
	Modul_str_4_img = ImgModulStructure.objects.filter(name = "4")
	Modul_str_3 = ModulStructure.objects.filter(id = "3")
	Modul_str_3_img = ImgModulStructure.objects.filter(name = "3")
	Modul_str_2 = ModulStructure.objects.filter(id = "2")
	Modul_str_2_img = ImgModulStructure.objects.filter(name = "2")
	Modul_str_1 = ModulStructure.objects.filter(id = "1")
	Modul_str_1_img = ImgModulStructure.objects.filter(name = "1")

	by_img = ImgBy.objects.filter(is_active = True)
	return render(request, "modulstr.html", locals())

def catalog_secur(request):
	Post_sec_7 = PostSecurity.objects.filter(id = "7")
	Post_sec_7_img = ImgPostSecurity.objects.filter(name = "7")
	Post_sec_6 = PostSecurity.objects.filter(id = "6")
	Post_sec_6_img = ImgPostSecurity.objects.filter(name = "6")
	Post_sec_5 = PostSecurity.objects.filter(id = "5")
	Post_sec_5_img = ImgPostSecurity.objects.filter(name = "5")
	Post_sec_4 = PostSecurity.objects.filter(id = "4")
	Post_sec_4_img = ImgPostSecurity.objects.filter(name = "4")
	Post_sec_3 = PostSecurity.objects.filter(id = "3")
	Post_sec_3_img = ImgPostSecurity.objects.filter(name = "3")
	Post_sec_2 = PostSecurity.objects.filter(id = "2")
	Post_sec_2_img = ImgPostSecurity.objects.filter(name = "2")
	Post_sec_1 = PostSecurity.objects.filter(id = "1")
	Post_sec_1_img = ImgPostSecurity.objects.filter(name = "1")

	by_img = ImgBy.objects.filter(is_active = True)
	return render(request, "postsec.html", locals())
