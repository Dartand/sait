from django.urls import path
from django.contrib import admin
from catalog.models import ModulStructure, PostSecurity
from StrSec import views

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import render
from django.views.generic import DetailView


urlpatterns = [
         path(r'universal-stroyspb/modulstr/', views.catalog_str, name = "modul"),
         path(r'', views.catalog_secur, name = "postsec"),
         path(r'universal-stroyspb/modulstr/int:pk>',DetailView.as_view(model =ModulStructure, template_name = "3_obiect.html"), name = "modulstr_detail"),
         path(r'<int:pk>',DetailView.as_view(model =PostSecurity, template_name = "4_obiect.html"), name = "postsec_detail"),

         # path(r'<int:pk>',DetailView.as_view(model =Bitovka, template_name = "1_obiect.html"), name = 'bitovka_detail')
    ]\
            + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
            + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)