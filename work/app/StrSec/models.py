from django.db import models
from catalog.models import ModulStructure, PostSecurity

class ImgModulStructure(models.Model):
	name = models.ForeignKey(ModulStructure, blank = True, null = True, default = True, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = "product images/")
	is_active = models.BooleanField(default = False)
	is_main = models.BooleanField(default = False)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Фотграфия модульных задний"
		verbose_name_plural = "Фотографии модульных зданий"


class ImgPostSecurity(models.Model):
	name = models.ForeignKey(PostSecurity, blank = True, null = True, default = True, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = "product images/")
	is_active = models.BooleanField(default = False)
	is_main = models.BooleanField(default = False)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Фотграфия постов охраны"
		verbose_name_plural = "Фотографии постов охраны"

class CharacteristicModulStructure(models.Model):
	name = models.ForeignKey(ModulStructure, blank = True, null = True, default = True, on_delete = models.CASCADE)
	title = models.CharField(max_length = 50)
	description = models.CharField(max_length = 250)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Характеристика Модульного здания"
		verbose_name_plural = "Характеристики  Модульного здания"