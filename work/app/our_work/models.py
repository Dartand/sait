from django.db import models


class OurWork(models.Model):
	name = models.CharField(max_length = 50)
	description = models.TextField(max_length = 1000)
	price = models.CharField(max_length = 50)

	def __str__(self):
		return "%s" % self.name

	class Meta():
		verbose_name = "Наша работа"
		verbose_name_plural = "Наши работы"


class ImgOurWork(models.Model):
	name = models.ForeignKey(OurWork, blank = True, null = True, default = True, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = "product images/")
	is_active = models.BooleanField(default = False)
	is_main = models.BooleanField(default = False)

	def __str__(self):
		return "%s" % self.name
	
	class Meta():
		verbose_name = "Фото наиших работ"
		verbose_name_plural = "Фото наших работ"

# Create your models here.
