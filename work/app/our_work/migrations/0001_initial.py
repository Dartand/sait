# Generated by Django 4.0.5 on 2022-07-01 11:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='OurWork',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50)),
                ('description', models.TextField(max_length=1000)),
            ],
            options={
                'verbose_name': 'Наша работа',
                'verbose_name_plural': 'Наши работы',
            },
        ),
        migrations.CreateModel(
            name='ImgOurWork',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='product images/')),
                ('name', models.ForeignKey(blank=True, default=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='our_work.ourwork')),
            ],
            options={
                'verbose_name': 'Фото наиших работ',
                'verbose_name_plural': 'Фото наших работ',
            },
        ),
    ]
