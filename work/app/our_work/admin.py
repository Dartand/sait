from django.contrib import admin
from  .models import *


class ImgOurWorkInline(admin.TabularInline):
	model = ImgOurWork


class ImgOurWorkAdmin(admin.ModelAdmin):
	list_display = [field.name for field in ImgOurWork._meta.fields]
	

	class Meta:
		model = ImgOurWork

admin.site.register(ImgOurWork, ImgOurWorkAdmin)


class OurWorkAdmin(admin.ModelAdmin):
	list_display = [field.name for field in OurWork._meta.fields]
	inlines =[ImgOurWorkInline]
	

	class Meta:
		model = OurWork

admin.site.register(OurWork, OurWorkAdmin)
