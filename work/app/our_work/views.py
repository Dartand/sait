from django.shortcuts import render
from .models import *

def our_work(request):
	work_img = ImgOurWork.objects.filter(is_active = True)
	return render(request, "our_work.html", locals())

