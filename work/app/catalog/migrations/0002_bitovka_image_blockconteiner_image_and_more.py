# Generated by Django 4.0.5 on 2022-07-14 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalog', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='bitovka',
            name='image',
            field=models.ImageField(default=0, upload_to='product images/'),
        ),
        migrations.AddField(
            model_name='blockconteiner',
            name='image',
            field=models.ImageField(default=0, upload_to='product images/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='modulstructure',
            name='image',
            field=models.ImageField(default=231, upload_to='product images/'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='postsecurity',
            name='image',
            field=models.ImageField(default=12321, upload_to='product images/'),
            preserve_default=False,
        ),
    ]
