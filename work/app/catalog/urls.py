from django.urls import path
from django.contrib import admin
from bitovka import views

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import render


urlpatterns = [
         path('', views.catalog, name = 'catalog'),
    ]\
            + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
            + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)