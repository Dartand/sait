from django.db import models

class BlockConteiner(models.Model):
	name = models.CharField(max_length = 50)
	count_size = models.DecimalField(max_digits = 2, decimal_places = 0)
	description = models.TextField(max_length = 500)
	image = models.ImageField(upload_to = "product images/")
	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Блок-контейнер"
		verbose_name_plural = "Блок-контейнеры"


class Bitovka(models.Model):
	name = models.CharField(max_length = 50)
	count_size = models.DecimalField(max_digits = 2, decimal_places = 0)
	description = models.TextField(max_length = 500)
	image = models.ImageField(upload_to = "product images/", default = 0)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Бытовка"
		verbose_name_plural = "Бытовки"




class PostSecurity( models.Model):
	size = models.CharField(max_length = 50)
	name = models.CharField(max_length = 50)
	image = models.ImageField(upload_to = "product images/")
	price_dvp = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_pvx = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_vagonka = models.DecimalField(max_digits = 7, decimal_places = 0)


	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Пост охраны"
		verbose_name_plural = "Посты охраны"

class ModulStructure(models.Model):
	image = models.ImageField(upload_to = "product images/")
	size = models.CharField(max_length = 50)
	name = models.CharField(max_length = 50)
	price_dvp = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_pvx = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_vagonka = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_mdf = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_LDCP = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_dvpo = models.DecimalField(max_digits = 7,  decimal_places = 0)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Модульное здание"
		verbose_name_plural = "Модульные здания"