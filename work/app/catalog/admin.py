from django.contrib import admin
from .models import *
from bitovka.models import *
from StrSec.models import *
from continer.models import *


class PriceConteinerInline(admin.TabularInline):
	model = PriceConteiner

class ImgConteinerInline(admin.TabularInline):
	model = ImgConteiner

class CharacteristicConteinerInline(admin.TabularInline):
	model = CharacteristicConteiner
"<------------------------------------------------->"

class PriceBitovkaInline(admin.TabularInline):
	model = PriceBitovka

class ImgBitovkaInline(admin.TabularInline):
	model = ImgBitovka
class CharacteristicBitovkaInline(admin.TabularInline):
	model = CharacteristicBitovka
"<------------------------------------------------->"

class ImgModulStructureInline(admin.TabularInline):
	model = ImgModulStructure

class CharacteristicModulStructureInline(admin.TabularInline):
	model = CharacteristicModulStructure

class ImgPostSecurityInline(admin.TabularInline):
	model = ImgPostSecurity

"<------------------------------------------------->"


class BlockConteinerAdmin (admin.ModelAdmin):
	list_display = [field.name for field in BlockConteiner._meta.fields]
	inlines =[PriceConteinerInline, ImgConteinerInline, CharacteristicConteinerInline]
	

	class Meta:
		model = BlockConteiner

admin.site.register(BlockConteiner, BlockConteinerAdmin)

"<------------------------------------------------->"

class BitovkaAdmin (admin.ModelAdmin):
	list_display = [field.name for field in Bitovka._meta.fields]

	inlines = [PriceBitovkaInline, ImgBitovkaInline, CharacteristicBitovkaInline]

	class Meta:
		model = Bitovka

admin.site.register(Bitovka, BitovkaAdmin)

"<------------------------------------------------->"

class ModulStructureAdmin (admin.ModelAdmin):
	list_display = [field.name for field in ModulStructure._meta.fields]
	inlines = [ImgModulStructureInline, CharacteristicModulStructureInline]
	
	class Meta:
		model = ModulStructure

admin.site.register(ModulStructure, ModulStructureAdmin)

"<------------------------------------------------->"

class PostSecurityAdmin (admin.ModelAdmin):
	list_display = [field.name for field in PostSecurity._meta.fields]
	inlines = [ImgPostSecurityInline]

	class Meta:
		model = PostSecurity

admin.site.register(PostSecurity, PostSecurityAdmin)