from django.db import models
from catalog.models import BlockConteiner


class PriceConteiner(models.Model):
	name = models.ForeignKey(BlockConteiner, blank = True, null = True, default = True, on_delete = models.CASCADE)
	size = models.CharField(max_length = 50)
	price_dvp = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_pvx = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_vagonka = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_mdf = models.DecimalField(max_digits = 7, decimal_places = 0)
	price_LDCP = models.DecimalField(max_digits = 7, decimal_places = 0)

	def __str__(self):
		return "%s, %s" % (self.name, self.size)

	class Meta:
		verbose_name = "Размер и цены контейнера"
		verbose_name_plural = "Размеры и цены контейнера"


class ImgConteiner(models.Model):
	name = models.ForeignKey(BlockConteiner, blank = True, null = True, default = True, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = "product images/")
	is_active = models.BooleanField(default = False)
	is_main = models.BooleanField(default = False)

	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Фотграфия контейнеров"
		verbose_name_plural = "Фотографии  контейнеров"

class CharacteristicConteiner(models.Model):
	name = models.ForeignKey(BlockConteiner, blank = True, null = True, default = True, on_delete = models.CASCADE)
	title = models.CharField(max_length = 50)
	description = models.CharField(max_length = 250)
	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Характеристика контейнеров"
		verbose_name_plural = "Характеристики  контейнеров"