from django.contrib import admin
from .models import *

class PriceConteinerAdmin (admin.ModelAdmin):
	list_display = [field.name for field in PriceConteiner._meta.fields]

	class Meta:
		model = PriceConteiner

admin.site.register(PriceConteiner, PriceConteinerAdmin)

class ImgConteinerAdmin (admin.ModelAdmin):
	list_display = [field.name for field in ImgConteiner._meta.fields]

	class Meta:
		model = ImgConteiner

admin.site.register(ImgConteiner, ImgConteinerAdmin)

class CharacteristicConteinerAdmin(admin.ModelAdmin):
	list_display = [field.name for field in CharacteristicConteiner._meta.fields]

	class Meta:
		model = CharacteristicConteiner

admin.site.register(CharacteristicConteiner, CharacteristicConteinerAdmin)