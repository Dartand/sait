from django.apps import AppConfig


class ContinerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'continer'

    verbose_name = "Контейнеры"
