from django.contrib import admin
from .models import *

class ImgByInline(admin.TabularInline):
	model = ImgBy

class СharacteristicByInline(admin.TabularInline):
	model = СharacteristicBy

"<------------------------------------------------------>"

class СharacteristicByAdmin(admin.ModelAdmin):
	list_display = [field.name for field in СharacteristicBy._meta.fields]

	class Meta:
		model = СharacteristicBy

admin.site.register(СharacteristicBy, СharacteristicByAdmin)


class ImgByAdmin(admin.ModelAdmin):
	list_display = [field.name for field in ImgBy._meta.fields]

	class Meta:
		model = ImgBy

admin.site.register(ImgBy, ImgByAdmin)

class ByAdmin(admin.ModelAdmin):
	list_display = [field.name for field in By._meta.fields]
	inlines =[ImgByInline, СharacteristicByInline]
	class Meta:
		model = By

admin.site.register(By, ByAdmin)
# Register your models here.

