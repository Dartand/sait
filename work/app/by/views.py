from django.shortcuts import render
from .models import *

def sale(request):
	by_img = ImgBy.objects.filter(is_active = True)
	return render(request, "sale.html", locals())


def base(request):
	by_img = ImgBy.objects.filter(is_active = True)
	return render(request, "main.html", locals())
