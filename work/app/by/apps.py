from django.apps import AppConfig


class ByConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'by'
