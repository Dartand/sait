from django.db import models

class By(models.Model):
	name =models.CharField(max_length = 50)
	new_price = models.DecimalField(max_digits = 7, decimal_places = 0)
	old_price = models.DecimalField(max_digits = 7, decimal_places = 0)
	ifo = models.TextField()


	def __str__(self):
		return "%s" % self.name

	class Meta:
		verbose_name = "Бу изделие"
		verbose_name_plural = "Бу изделия"



class ImgBy(models.Model):
	name = models.ForeignKey(By, blank = True, null = True, default = True, on_delete = models.CASCADE)
	image = models.ImageField(upload_to = "static/product images/")
	is_active = models.BooleanField(default = False)
	is_main = models.BooleanField(default = False)
	def __str__(self):
		return "%s" % self.name


	class Meta():
		verbose_name = "Фото бу изделия"
		verbose_name_plural = "Фото бу изделий"

class СharacteristicBy(models.Model):
	name = models.ForeignKey(By, blank = True, null = True, default = True, on_delete = models.CASCADE)
	charakter = models.CharField(max_length = 300)
	is_main = models.BooleanField(default = False)

	def __str__(self):
		return "%s" % self.name

	class Meta():
		verbose_name = "Характеристика бу изделий"
		verbose_name_plural = "Характеристики бу изделий"
