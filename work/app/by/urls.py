from django.urls import path
from django.contrib import admin
from by import views
from by.models import *

from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import render
from django.views.generic import DetailView


urlpatterns = [
   path('', views.sale, name = 'name'),
   path('universal-stroyspb/', views.base, name = 'base'),
   path(r'<int:pk>',DetailView.as_view(model =By, template_name = "1_by.html"), name = "by_detail")

    ]\
            + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
            + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)