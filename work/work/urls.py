"""work URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings
from django.shortcuts import render


def main(request):
    return render(request, "main.html")

def contact(request):
    return render(request, "contact.html")

def garantii(request):
    return render(request, "garantia.html")

def compania(request):
    return render(request, "Company.html")

def dostavka(request):
    return render(request, "dostavka.html")






urlpatterns = [
    path(r'universal-stroyspb/admin/', admin.site.urls),
    path(r'universal-stroyspb/bitovki/', include('app.bitovka.urls'), name = "bitovka"),
    path(r'universal-stroyspb/bitovki/',include('app.bitovka.urls'), name = 'bitovka_detail'),
    path(r'universal-stroyspb/conteineri/', include('app.continer.urls'), name = "coteiner"),
    path(r'universal-stroyspb/conteineri/', include('app.continer.urls'), name = "coteiner_detail"),
    path(r'', include('app.StrSec.urls'), name = "modul"),
    path(r'', include('app.StrSec.urls'), name = "modulstr_detail"),
    path(r'universal-stroyspb/postsec/', include('app.StrSec.urls'), name = "postsec"),
    path(r'universal-stroyspb/postsec/', include('app.StrSec.urls'), name = "postsec_detail"),
    path(r'', include('app.by.urls'), name = 'base'),
    path(r'universal-stroyspb/contaсts/', contact, name = 'contacts'),
    path(r'universal-stroyspb/garantii/', garantii, name = 'garantii'),
    path(r'universal-stroyspb/compania/', compania, name = 'compania'),
    path(r'universal-stroyspb/dostavka/', dostavka, name = 'dostavka'),
    path(r'universal-stroyspb/work/', include('app.our_work.urls')),
    path(r'universal-stroyspb/work/', include('app.our_work.urls') , name = "work_detail"),
    path(r'universal-stroyspb/bani/', include('app.bani.urls'), name = 'bani' ),
    path(r'universal-stroyspb/sale/', include('app.by.urls')),
    path(r'universal-stroyspb/arenda/', include('app.arenda.urls')),
    path(r'universal-stroyspb/arenda/', include('app.arenda.urls'), name = "arenda_detail"),
    path(r'universal-stroyspb/sale/', include('app.by.urls'), name = "1_by"),
    path(r'universal-stroyspb/news/', include('app.news.urls'), name = "news_detail"),
    path(r'universal-stroyspb/news/', include('app.news.urls'))

]\
            + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
            + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
