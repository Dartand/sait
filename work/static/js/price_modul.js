const totalPriceElement_1 = document.querySelector('#price_1');
const totalPriceElement_2 = document.querySelector('#price_2');
const totalPriceElement_3 = document.querySelector('#price_3');
const totalPriceElement_4 = document.querySelector('#price_4');
const totalPriceElement_5 = document.querySelector('#price_5');
const totalPriceElement_6 = document.querySelector('#price_6');
const totalPriceElement_7 = document.querySelector('#price_7');
const inputs = document.querySelectorAll('input');



const radioMat_price_1 = document.querySelectorAll('input[name="mat_price_1"]');

const radioMat_price_2 = document.querySelectorAll('input[name="mat_price_2"]');

const radioMat_price_3 = document.querySelectorAll('input[name="mat_price_3"]');

const radioMat_price_4 = document.querySelectorAll('input[name="mat_price_4"]');

const radioMat_price_5 = document.querySelectorAll('input[name="mat_price_5"]');

const radioMat_price_6 = document.querySelectorAll('input[name="mat_price_6"]');

const radioMat_price_7 = document.querySelectorAll('input[name="mat_price_7"]');


function calculate_1() {
    for ( const radio of radioMat_price_1) {
        if (radio.checked) {
            totalPrice_1 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_1.innerText = formatter.format(totalPrice_1);
}
function calculate_2() {
    let totalPrice_2 = 0
    for (const radio of radioMat_price_2) {
        if (radio.checked) {
            totalPrice_2 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_2.innerText = formatter.format(totalPrice_2);
}}
function calculate_3() {
    let totalPrice_3 = 0
    for (const radio of radioMat_price_3) {
        if (radio.checked) {
            totalPrice_3 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_3.innerText = formatter.format(totalPrice_3);
}}

function calculate_4() {
    let totalPrice_4 = 0
    for (const radio of radioMat_price_4) {
        if (radio.checked) {
            totalPrice_4 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_4.innerText = formatter.format(totalPrice_4);
}
function calculate_5() {
    let totalPrice_5 = 0
    for (const radio of radioMat_price_5) {
        if (radio.checked) {
            totalPrice_5 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_5.innerText = formatter.format(totalPrice_5);
}
function calculate_6() {
    let totalPrice_6 = 0
    for (const radio of radioMat_price_6) {
        if (radio.checked) {
            totalPrice_6 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_6.innerText = formatter.format(totalPrice_6);
}
function calculate_7() {
    let totalPrice_7 = 0
    for (const radio of radioMat_price_7) {
        if (radio.checked) {
            totalPrice_7 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_7.innerText = formatter.format(totalPrice_7);
}

 calculate_1();
 calculate_2();
 calculate_3();
 calculate_4();
 calculate_5();
 calculate_6();
 calculate_7();

for (const input of inputs) {
	input.addEventListener('input', function () {
		calculate_1();
        calculate_2();
        calculate_3();
        calculate_4();
        calculate_5();
        calculate_6();
        calculate_7();
	});
}
