const totalPriceElement_M_1 = document.querySelector('#price10');
const totalPriceElement_M_2 = document.querySelector('#price20');
const totalPriceElement_M_3 = document.querySelector('#price30');
const totalPriceElement_M_4 = document.querySelector('#price40');
const totalPriceElement_M_5 = document.querySelector('#price50');

const inputs = document.querySelectorAll('input');



const radioMat_price_M_1 = document.querySelectorAll('input[name="mat_price_M_1"]');

const radioMat_price_M_2 = document.querySelectorAll('input[name="mat_price_M_2"]');

const radioMat_price_M_3 = document.querySelectorAll('input[name="mat_price_M_3"]');

const radioMat_price_M_4 = document.querySelectorAll('input[name="mat_price_M_4"]');

const radioMat_price_M_5 = document.querySelectorAll('input[name="mat_price_M_5"]');




function calculate_1() {
    for ( const radio of radioMat_price_M_1) {
        if (radio.checked) {
            totalPrice_1 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_M_1.innerText = formatter.format(totalPrice_1);
}
function calculate_2() {
    let totalPrice_2 = 0
    for (const radio of radioMat_price_M_2) {
        if (radio.checked) {
            totalPrice_2 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_M_2.innerText = formatter.format(totalPrice_2);
}}
function calculate_3() {
    let totalPrice_3 = 0
    for (const radio of radioMat_price_M_3) {
        if (radio.checked) {
            totalPrice_3 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_M_3.innerText = formatter.format(totalPrice_3);
}}

function calculate_4() {
    let totalPrice_4 = 0
    for (const radio of radioMat_price_M_4) {
        if (radio.checked) {
            totalPrice_4 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_M_4.innerText = formatter.format(totalPrice_4);
}

function calculate_5() {
    let totalPrice_5 = 0
    for (const radio of radioMat_price_M_5) {
        if (radio.checked) {
            totalPrice_5 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}


    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_M_5.innerText = formatter.format(totalPrice_5);
}


 calculate_1();
 calculate_2();
 calculate_3();
 calculate_4();
 calculate_5();

for (const input of inputs) {
	input.addEventListener('input', function () {
		calculate_1();
        calculate_2();
        calculate_3();
        calculate_4();
        calculate_5();

	});
}
