const totalPriceElement_bitovka_1 = document.querySelector('#price1');
const totalPriceElement_bitovka_2 = document.querySelector('#price2');
const totalPriceElement_bitovka_3 = document.querySelector('#price3');
const totalPriceElement_bitovka_4 = document.querySelector('#price4');

const inputs = document.querySelectorAll('input');



const radioSize_bitovka_1 = document.querySelectorAll('input[name="size_bitovka_1"]');
const radioMat_bitovka_1 = document.querySelectorAll('input[name="mat_bitovka_1"]');
const radioSize_bitovka_2 = document.querySelectorAll('input[name="size_bitovka_2"]');
const radioMat_bitovka_2 = document.querySelectorAll('input[name="mat_bitovka_2"]');
const radioSize_bitovka_3 = document.querySelectorAll('input[name="size_bitovka_3"]');
const radioMat_bitovka_3 = document.querySelectorAll('input[name="mat_bitovka_3"]');
const radioSize_bitovka_4 = document.querySelectorAll('input[name="size_bitovka_4"]');
const radioMat_bitovka_4 = document.querySelectorAll('input[name="mat_bitovka_4"]');

function calculate_1() {
    let totalPrice_1 = 0
    for ( const radio of radioSize_bitovka_1) {
        if (radio.checked) {
            totalPrice_1 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_bitovka_1) {
        if (radio.checked) {
            totalPrice_1 = totalPrice_1 + (totalPrice_1 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_bitovka_1.innerText = formatter.format(totalPrice_1);
}
function calculate_2() {
    let totalPrice_2 = 0
    for (const radio of radioSize_bitovka_2) {
        if (radio.checked) {
            totalPrice_2 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_bitovka_2) {
        if (radio.checked) {
            totalPrice_2 = totalPrice_2 + (totalPrice_2 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_bitovka_2.innerText = formatter.format(totalPrice_2);
}
function calculate_3() {
    let totalPrice_3 = 0
    for (const radio of radioSize_bitovka_3) {
        if (radio.checked) {
            totalPrice_3 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_bitovka_3) {
        if (radio.checked) {
            totalPrice_3 = totalPrice_3 + (totalPrice_3 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_bitovka_3.innerText = formatter.format(totalPrice_3);
}

function calculate_4() {
    let totalPrice_4 = 0
    for (const radio of radioSize_bitovka_4) {
        if (radio.checked) {
            totalPrice_4 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_bitovka_4) {
        if (radio.checked) {
            totalPrice_4 = totalPrice_4 + (totalPrice_4 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_bitovka_4.innerText = formatter.format(totalPrice_4);
}


 calculate_1();
 calculate_2();
 calculate_3();
 calculate_4();


for (const input of inputs) {
	input.addEventListener('input', function () {
		calculate_1();
    calculate_2();
    calculate_3();
    calculate_4();

	});
}
