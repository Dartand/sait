const totalPriceElement_1 = document.querySelector('#price_1');
const totalPriceElement_2 = document.querySelector('#price_2');
const totalPriceElement_3 = document.querySelector('#price_3');
const totalPriceElement_4 = document.querySelector('#price_4');
const totalPriceElement_5 = document.querySelector('#price_5');
const totalPriceElement_6 = document.querySelector('#price_6');
const totalPriceElement_7 = document.querySelector('#price_7');
const inputs = document.querySelectorAll('input');



const radioSize_1 = document.querySelectorAll('input[name="size_1"]');
const radioMat_1 = document.querySelectorAll('input[name="mat_1"]');
const radioSize_2 = document.querySelectorAll('input[name="size_2"]');
const radioMat_2 = document.querySelectorAll('input[name="mat_2"]');
const radioSize_3 = document.querySelectorAll('input[name="size_3"]');
const radioMat_3 = document.querySelectorAll('input[name="mat_3"]');
const radioSize_4 = document.querySelectorAll('input[name="size_4"]');
const radioMat_4 = document.querySelectorAll('input[name="mat_4"]');
const radioSize_5 = document.querySelectorAll('input[name="size_5"]');
const radioMat_5 = document.querySelectorAll('input[name="mat_5"]');
const radioSize_6 = document.querySelectorAll('input[name="size_6"]');
const radioMat_6 = document.querySelectorAll('input[name="mat_6"]');
const radioSize_7 = document.querySelectorAll('input[name="size_7"]');
const radioMat_7 = document.querySelectorAll('input[name="mat_7"]');

function calculate_1() {
    for ( const radio of radioSize_1) {
        if (radio.checked) {
            totalPrice_1 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_1) {
        if (radio.checked) {
            totalPrice_1 = totalPrice_1 + (totalPrice_1 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_1.innerText = formatter.format(totalPrice_1);
}
function calculate_2() {
    let totalPrice_2 = 0
    for (const radio of radioSize_2) {
        if (radio.checked) {
            totalPrice_2 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_2) {
        if (radio.checked) {
            totalPrice_2 = totalPrice_2 + (totalPrice_2 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_2.innerText = formatter.format(totalPrice_2);
}
function calculate_3() {
    let totalPrice_3 = 0
    for (const radio of radioSize_3) {
        if (radio.checked) {
            totalPrice_3 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_3) {
        if (radio.checked) {
            totalPrice_3 = totalPrice_3 + (totalPrice_3 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_3.innerText = formatter.format(totalPrice_3);
}

function calculate_4() {
    let totalPrice_4 = 0
    for (const radio of radioSize_4) {
        if (radio.checked) {
            totalPrice_4 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_4) {
        if (radio.checked) {
            totalPrice_4 = totalPrice_4 + (totalPrice_4 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_4.innerText = formatter.format(totalPrice_4);
}
function calculate_5() {
    let totalPrice_5 = 0
    for (const radio of radioSize_5) {
        if (radio.checked) {
            totalPrice_5 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_5) {
        if (radio.checked) {
            totalPrice_5 = totalPrice_5 + (totalPrice_5 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_5.innerText = formatter.format(totalPrice_5);
}
function calculate_6() {
    let totalPrice_6 = 0
    for (const radio of radioSize_6) {
        if (radio.checked) {
            totalPrice_6 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_6) {
        if (radio.checked) {
            totalPrice_6 = totalPrice_6 + (totalPrice_6 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_6.innerText = formatter.format(totalPrice_6);
}
function calculate_7() {
    let totalPrice_7 = 0
    for (const radio of radioSize_7) {
        if (radio.checked) {
            totalPrice_7 = parseInt(radio.value); // 390 000 * 0.8 = 350 000
    }
}
    for (const radio of radioMat_7) {
        if (radio.checked) {
            totalPrice_7 = totalPrice_7 + (totalPrice_7 * parseFloat(radio.value)); // 390 000 * 0.8 = 350 000
    }
}

    const formatter = new Intl.NumberFormat('Ru');
    totalPriceElement_7.innerText = formatter.format(totalPrice_7);
}

 calculate_1();
 calculate_2();
 calculate_3();
 calculate_4();
 calculate_5();
 calculate_6();
 calculate_7();

for (const input of inputs) {
	input.addEventListener('input', function () {
		calculate_1();
    calculate_2();
    calculate_3();
    calculate_4();
    calculate_5();
    calculate_6();
    calculate_7();
	});
}
